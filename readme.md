+ es?
+ cover and modify
+ strcutruzier
+ testy mservicow
 + e2e
 + konrakty
+ puchnące serwsisy

- bartyzel - event storming
- c4
- https://structurizr.com/ https://github.com/structurizr/java


given:
- uzytkownik jest zalogowany w systemie
- produkt jest dostępny
- uzytkownik wchodzi na strone aukcji z linka bezposredniego

when:
- uzytkownik klika kup teraz
- produkt zostal zarezerwowany na 10 minut
- uzytkownik wybiera opcje dostawy

then:


/users/123
1. DELETE /users/123
2. PUT /users/123/status -> {newStatus:"DISABLED"}
3*. PUT /users/123 -> {id, name, status:"DISABLED"}
4. POST /users/123/disables
   GET /users/123/disables == [..,..,..]
   POST /users/123/activations

1. POST /policies/123/cancellations
2. DELETE /policies/123

/users/commands == JSONRPC

- schema stiching
 - https://www.apollographql.com/docs/apollo-server/federation/migrating-from-stitching/
 - https://www.apollographql.com/docs/apollo-server/federation/introduction/


Junior:
 if (false) {
   ....
 }

Mid:
if (1 != 1) {
   ....
}

Senior:
if (isAsyncProcessingEnabled()) {
   ....
}

CI:
bool isAsyncProcessingEnabled() {
	return false;
}

CD:

bool isAsyncProcessingEnabled() {
	return jdbcTemplate.get("select value from toogles where name='ASYNC_PROCESSING'");
}

void addRoleToUser(UserId, List<Role>)
boolean addRoleToUser(UserId, Role...)

addRoleToUser(UserId, newRole, newRole2);

https://launchdarkly.com/

exam.devskiller.com/api/exams/THYA-7hs6-AJAD/sjhjads

https://github.com/zalando/logbook

POST /users/12311324313/changePassword
{oldPass:"dassdasd", newPass1:"aasdaa", newPass2: "asdadas"}

http://www.brendangregg.com/

http://www.brendangregg.com/flamegraphs.html

https://sekurak.pl/darmowy-ebook-o-bezpieczenstwie-jwt-json-web-token-pierwszy-tekst-z-papierowej-ksiazki-sekuraka/

https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/



class Notification {
	User user;

	User getUser() {
	  return user
	}	
}

refactor =>

class Notification {
	Long user;
	@Transient UserDto userDto;

	UserDto getUser() {
	  return userDto;
	}
}

notificationFacade.getNotification(id) {
	notif = db.findNotification(id)
	notif.setUserDto(userRepo.find(notif.user));
	return notif;
}


Konfiguracja:
jasiek@objectivity.com
rambo23@objectivity.com
slodziak@objectivity.com
Konwencja:
imie.nazwisko@objectivity.com

ustron, wisla, zywiec
crm1, crm2, crm3

user: crm-user
home: /home/crm
service: crm-servoce
sd: crm
repo: objectivity/apps/crm
app: crm.jar

copy: src=target/{{appName}}.jar dest=/home/{{appName}}/{{appName}}.jar owner={{appName}}-user group=apps

archiveArtifacts=target/{{appName}}.jar

- name: Remove the user 'johnd'
  user:
    name: johnd
    state: absent
    remove: yes

- name: install the latest version of Apache
  yum:
    name: httpd
    state: latest
