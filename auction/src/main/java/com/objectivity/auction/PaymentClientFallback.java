package com.objectivity.auction;

import org.springframework.stereotype.Component;

@Component
class PaymentClientFallback implements PaymentClient {

	@Override
	public Payment getPaymentStatus(String id) {
		return null;
	}
}
