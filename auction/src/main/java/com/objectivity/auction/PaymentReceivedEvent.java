package com.objectivity.auction;

class PaymentReceivedEvent {

	private String paymentId;

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentId() {
		return paymentId;
	}
}
