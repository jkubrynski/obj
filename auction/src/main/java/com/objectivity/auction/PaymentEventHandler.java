package com.objectivity.auction;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class PaymentEventHandler {

	@StreamListener(PaymentsQueue.PAYMENTS_QUEUE_NAME)
	void handlePaymentEvent(PaymentReceivedEvent event) {
		System.out.println(event.getPaymentId());
	}
}
