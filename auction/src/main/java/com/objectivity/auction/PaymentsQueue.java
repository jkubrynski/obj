package com.objectivity.auction;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

interface PaymentsQueue {

	String PAYMENTS_QUEUE_NAME = "paymentsQueue";

	@Input(PAYMENTS_QUEUE_NAME)
	MessageChannel queue();
}
