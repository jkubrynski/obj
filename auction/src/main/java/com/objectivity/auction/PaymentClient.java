package com.objectivity.auction;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "payment", fallback = PaymentClientFallback.class, decode404 = true)
interface PaymentClient {

	@GetMapping(value = "/payments/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Payment getPaymentStatus(@PathVariable("id") String id);
}
