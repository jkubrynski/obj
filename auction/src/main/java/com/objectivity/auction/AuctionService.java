package com.objectivity.auction;

import org.springframework.stereotype.Service;

@Service
class AuctionService {

	private final PaymentClient paymentClient;

	AuctionService(PaymentClient paymentClient) {
		this.paymentClient = paymentClient;
	}

	String getAuction(String auctionId) {
		return "PaymentStatus=" + paymentClient.getPaymentStatus(auctionId).getStatus();
	}
}
