package com.objectivity.auction;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = "com.objectivity:payment:+:stubs")
public class PaymentClientTest {

	@Autowired
	PaymentClient paymentClient;

	@Test
	public void shouldRetrievePaymentStatus() {
		// when
		Payment paymentStatus = paymentClient.getPaymentStatus("123");
		// then
		assertThat(paymentStatus).isNotNull();
		assertThat(paymentStatus.getPaymentId()).isEqualTo("123");
		assertThat(paymentStatus.getStatus()).isEqualTo("OK");
	}
}