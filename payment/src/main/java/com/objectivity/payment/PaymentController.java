package com.objectivity.payment;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payments")
class PaymentController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentWebhookHandler paymentWebhookHandler;
	private final PaymentFacade paymentFacade;

	PaymentController(PaymentWebhookHandler paymentWebhookHandler, PaymentFacade paymentFacade) {
		this.paymentWebhookHandler = paymentWebhookHandler;
		this.paymentFacade = paymentFacade;
	}

	@GetMapping("/{paymentId}")
	PaymentResponse getPaymentStatus(@PathVariable String paymentId) {
		LOG.info("Checking payment status [paymentId={}]", paymentId);
		return paymentFacade.retrievePaymentStatus(paymentId);
	}

	@PutMapping("/{paymentId}")
	void paymentReceived(@PathVariable String paymentId) {
		paymentWebhookHandler.paymentReceived(paymentId);
	}
}
