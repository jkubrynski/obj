package com.objectivity.payment;

class PaymentResponse {

	private final String paymentId;
	private final PaymentStatus paymentStatus;

	PaymentResponse(String paymentId, PaymentStatus paymentStatus) {
		this.paymentId = paymentId;
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public PaymentStatus getStatus() {
		return paymentStatus;
	}
}
