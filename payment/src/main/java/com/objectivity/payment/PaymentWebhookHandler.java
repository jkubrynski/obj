package com.objectivity.payment;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
class PaymentWebhookHandler {

	private final PaymentsQueue paymentsQueue;

	PaymentWebhookHandler(PaymentsQueue paymentsQueue) {
		this.paymentsQueue = paymentsQueue;
	}

	void paymentReceived(String paymentId) {
		MessageHeaders messageHeaders = new MessageHeaders(Map.of(
				MessageHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE
		));
		paymentsQueue.queue().send(
			MessageBuilder.createMessage(new PaymentReceivedEvent(paymentId), messageHeaders)
		);
	}
}
