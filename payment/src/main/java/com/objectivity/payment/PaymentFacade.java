package com.objectivity.payment;

import org.springframework.stereotype.Service;

@Service
class PaymentFacade {

	PaymentResponse retrievePaymentStatus(String paymentId) {
		return new PaymentResponse(
				paymentId, getPaymentStatus(paymentId)
		);
	}

	private PaymentStatus getPaymentStatus(String paymentId) {
		return paymentId.length() > 2 ? PaymentStatus.OK : PaymentStatus.FAILED;
	}
}
