package com.objectivity.payment;

class PaymentReceivedEvent {

	private final String paymentId;

	PaymentReceivedEvent(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentId() {
		return paymentId;
	}
}
