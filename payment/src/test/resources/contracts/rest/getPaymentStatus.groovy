package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		method('GET')
		url('/payments/123')
		headers {
			accept(applicationJsonUtf8())
		}
	}

	response {
		status(200)
		headers {
			contentType(applicationJsonUtf8())
		}
		body(
			'paymentId':'123',
			'status': 'OK'
		)
	}
}