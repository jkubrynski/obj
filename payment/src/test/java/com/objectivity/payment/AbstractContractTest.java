package com.objectivity.payment;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;

abstract class AbstractContractTest {

	@Before
	public void setUp() throws Exception {
		PaymentFacade paymentFacade = Mockito.mock(PaymentFacade.class);

		Mockito.when(paymentFacade.retrievePaymentStatus("123")).thenReturn(new PaymentResponse("123", PaymentStatus.OK));

		RestAssuredMockMvc.standaloneSetup(new PaymentController(null, paymentFacade));
	}
}
